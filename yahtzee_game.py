# -*- coding: utf-8 -*-
"""ML_Assignment_3.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1zpDn1dj1nr0RVOZtYnN2-UN8lvfIbUKh

Yahtzee Game
"""

import random

def initialize_game():
    return [0] * 13

def roll_dice():
    return [random.randint(1, 6) for _ in range(5)]

def select_dice_to_keep(dice):
    print("Current roll:", dice)
    selected_dice = input("Enter indices of dice to keep (e.g., 1 3 5): ").split()
    return [int(i) - 1 for i in selected_dice]

def reroll_dice(dice, dice_to_reroll):
    for i in dice_to_reroll:
        dice[i] = random.randint(1, 6)
    return dice

def display_scorecard(scorecard):
    print("\nScorecard:")
    for i, score in enumerate(scorecard):
        print(f"{i + 1}. {score}")

def select_category_to_score(scorecard):
    print("\nAvailable categories:")
    for i, score in enumerate(scorecard):
        if score == 0:
            print(f"{i + 1}. Category {i + 1}")
    category = int(input("Choose a category to score: ")) - 1
    return category

def validate_category_selection(category, scorecard):
    return scorecard[category] == 0

def calculate_score(dice, category):
    if category < 6:
        return sum(d for d in dice if d == category + 1)
    elif category == 6:  # Three of a Kind
        return sum(dice)
    elif category == 7:  # Four of a Kind
        for d in set(dice):
            if dice.count(d) >= 4:
                return sum(dice)
        return 0
    elif category == 8:  # Full House
        if len(set(dice)) == 2 and (dice.count(dice[0]) == 2 or dice.count(dice[0]) == 3):
            return 25
        return 0
    elif category == 9:  # Small Straight
        if any([dice.count(i) >= 1 and dice.count(i + 1) >= 1 and dice.count(i + 2) >= 1 and dice.count(i + 3) >= 1 for i in range(2)]):
            return 30
        return 0
    elif category == 10:  # Large Straight
        if any([dice.count(i) >= 1 for i in range(1, 6)]):
            return 40
        return 0
    elif category == 11:  # Chance
        return sum(dice)
    elif category == 12:  # Yahtzee
        if len(set(dice)) == 1:
            return 50
        return 0

def update_scorecard(scorecard, category, score):
    scorecard[category] = score
    return scorecard

def check_end_game(scorecard):
    return all(scorecard)

def calculate_total_score(scorecard):
    return sum(scorecard)

def display_winner(player_score, computer_score):
    print("\nGame Over!")
    print("Your score:", player_score)
    print("Computer's score:", computer_score)
    if player_score > computer_score:
        print("Congratulations! You win!")
    elif player_score < computer_score:
        print("Sorry, you lose!")
    else:
        print("It's a tie!")

def main():
    print("Welcome to Yahtzee!")
    scorecard = initialize_game()
    player_score = 0
    computer_score = 0

    while not check_end_game(scorecard):
        input("Press Enter to roll the dice...")
        player_dice = roll_dice()
        print("Your roll:", player_dice)

        dice_to_keep = select_dice_to_keep(player_dice)
        player_dice = reroll_dice(player_dice, dice_to_keep)
        print("Your new roll:", player_dice)

        category = select_category_to_score(scorecard)
        while not validate_category_selection(category, scorecard):
            print("Invalid category! Please choose an empty category.")
            category = select_category_to_score(scorecard)

        score = calculate_score(player_dice, category)
        scorecard = update_scorecard(scorecard, category, score)
        player_score = calculate_total_score(scorecard)

        print("Your scorecard:")
        display_scorecard(scorecard)
        print("Your total score:", player_score)

        input("Press Enter to let the computer take its turn...")

        computer_dice = roll_dice()
        computer_category = random.choice([i for i, score in enumerate(scorecard) if score == 0])
        computer_score = calculate_score(computer_dice, computer_category)
        scorecard = update_scorecard(scorecard, computer_category, computer_score)

        print("Computer's scorecard:")
        display_scorecard(scorecard)
        print("Computer's total score:", computer_score)

    display_winner(player_score, computer_score)

if __name__ == "__main__":
    main()

